import { OrderItem } from 'src/orders/entities/order-item';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  OneToMany,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column({
    type: 'float',
  })
  price: number;

  @CreateDateColumn()
  createDate: Date;

  @UpdateDateColumn()
  updatedData: Date;

  @DeleteDateColumn()
  deletedData: Date;

  @OneToMany(() => OrderItem, (orderItems) => orderItems.product)
  orderItems: OrderItem[];
}
